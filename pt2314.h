////////////////////////////////////////////////////////////////////////////////
// PT2314 I2C 4-input Audio Processor with Volume, Bass, Treble, and Balance.
//
//
// Required the Wire.h library to be included in your Arduino Sketch.
//
// By Andrew Wyatt
// retrojdm.com
//

#ifndef PT2314_H
#define PT2314_H

#include "Arduino.h"
#include "Wire.h"


////////////////////////////////////////////////////////////////////////////////
// Debugging
// Uncomment this line if you want to see debugging information on the serial monitor.
// Note: You'll have to call Serial.begin(9600) somewhere in your sketch.
//#define PT2314_SERIAL_MONITOR


////////////////////////////////////////////////////////////////////////////////
// Constants

const int   PT2314_ADDR             = 0x44;

const byte  CHANNEL_0               = 0x00;
const byte  CHANNEL_1               = 0x01;
const byte  CHANNEL_2               = 0x02;
const byte  CHANNEL_3               = 0x03;

const byte  COMMAND_VOL_CTRL        = 0x00; // Master Volume (0..63, 63 = Mute)
const byte  COMMAND_L_ATTENUATOR    = 0xC0; // L attenu      (0..31)
const byte  COMMAND_R_ATTENUATOR    = 0xE0; // R attenu      (0..31)
const byte  COMMAND_AUDIO_SWITCH    = 0x5C; // Audio switch  (0..3)
const byte  COMMAND_BASS_CONTROL    = 0x60; // Bass          (0..31, None = 15)
const byte  COMMAND_TREBLE_CONTROL  = 0x70; // Treble        (0..31, None = 15)

const byte  PT2314_SOURCE_MIN       =   0;
const byte  PT2314_SOURCE_MAX       =   3;
const byte  PT2314_VOLUME_MIN       =   0;
const byte  PT2314_VOLUME_MAX       =  63;
const int   PT2314_BASS_MIN         =  -7;
const int   PT2314_BASS_MAX         =   7;
const int   PT2314_BASS_CMD_MIN     =   0;
const int   PT2314_BASS_CMD_MID     =   7;
const int   PT2314_BASS_CMD_MAX     =  15;
const int   PT2314_TREBLE_MIN       =  -7;
const int   PT2314_TREBLE_MAX       =   7;
const int   PT2314_TREBLE_CMD_MIN   =   0;
const int   PT2314_TREBLE_CMD_MID   =   7;
const int   PT2314_TREBLE_CMD_MAX   =  15;
const int   PT2314_BALANCE_MIN      = -31;
const int   PT2314_BALANCE_MAX      =  31;


////////////////////////////////////////////////////////////////////////////////
// Class definition

class PT2314 {
    
public:
    PT2314              ();
    void    setSource   (byte source);      // 0..3
    void    setSource   (byte source, bool loudness, byte gain); // Source: 0..3, Loudness on/off, gain: 0=+11.25dB, 1=+7.5dB, 2=+3.75dB, 3=0dB
    void    setVolume   (byte vol);         // 0..63
    void    setBass     (int intBass);      // -7..7
    void    setTreble   (int intTreble);    // -7..7
    void    setBalance  (int intBalance);   // -31..31

private:
    void    write       (byte command, byte val);
};

#endif