#include "PT2314.h"

////////////////////////////////////////////////////////////////////////////////
// Class constructor

PT2314::PT2314()
{

}


////////////////////////////////////////////////////////////////////////////////
// Public functions

void PT2314::setSource(byte source)
{ 
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print("PT2314::setSource(");
        Serial.print(source);
        Serial.print(") | source = ");
    #endif
    source = constrain(source, PT2314_SOURCE_MIN, PT2314_SOURCE_MAX);
    #ifdef PT2314_SERIAL_MONITOR
        Serial.println(source);
    #endif
    write(COMMAND_AUDIO_SWITCH, source);    
}


// source:      0..3
// loundness:   true/false
// gain:        0 = +11.25 dB
//              1 = + 7.5  dB
//              2 =   3.75 dB
//              3 =   0    dB
void PT2314::setSource(byte source, bool loudness, byte gain)
{
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print("PT2314::setSource(");
        Serial.print(source);
        Serial.print(", ");
        Serial.print(loudness);
        Serial.print(", ");
        Serial.print(gain);
        Serial.print(") | command = ");
    #endif
    source          = constrain(source, PT2314_SOURCE_MIN, PT2314_SOURCE_MAX);
    byte loud       = loudness ? 1 : 0;
    gain            = constrain(gain, 0, 3);
    byte command    = source | (loud << 2) | (gain << 3);
    #ifdef PT2314_SERIAL_MONITOR
        Serial.println(command);
    #endif
    write(COMMAND_AUDIO_SWITCH, source);    
}



void PT2314::setVolume(byte vol)
{
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print("PT2314::setVolume(");
        Serial.print(vol);
        Serial.print(") | vol = ");
    #endif
    vol = constrain(vol, PT2314_VOLUME_MIN, PT2314_VOLUME_MAX);
    #ifdef PT2314_SERIAL_MONITOR
        Serial.println(vol);
    #endif
    write(COMMAND_VOL_CTRL, PT2314_VOLUME_MAX - vol);
}


void PT2314::setBass(int bass)
{  
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print("PT2314::setBass(");
        Serial.print(bass);
        Serial.print(") | bass = ");
    #endif
    bass       = constrain(bass, PT2314_BASS_MIN, PT2314_BASS_MAX);
    int value  = (bass < 0)
        ? map(bass, PT2314_BASS_MIN, 0, PT2314_BASS_CMD_MIN, PT2314_BASS_CMD_MID)
        : map(bass, 0, PT2314_BASS_MAX, PT2314_BASS_CMD_MAX, PT2314_BASS_CMD_MID + 1);
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print(bass);
        Serial.print(", value = ");
        Serial.println(value);
    #endif
    write(COMMAND_BASS_CONTROL, value);
}


void PT2314::setTreble(int treble)
{    
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print("PT2314::setTreble(");
        Serial.print(treble);
        Serial.print(") | treble = ");
    #endif
    treble     = constrain(treble, PT2314_TREBLE_MIN, PT2314_TREBLE_MAX);
    int value  = (treble < 0)
        ? map(treble, PT2314_TREBLE_MIN, 0, PT2314_TREBLE_CMD_MIN, PT2314_TREBLE_CMD_MID)
        : map(treble, 0, PT2314_TREBLE_MAX, PT2314_TREBLE_CMD_MAX, PT2314_TREBLE_CMD_MID + 1);
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print(treble);
        Serial.print(", value = ");
        Serial.println(value);
    #endif
    write(COMMAND_TREBLE_CONTROL, value);
}


void PT2314::setBalance(int balance)
{ 
    #ifdef PT2314_SERIAL_MONITOR
        Serial.print("PT2314::setBalance(");
        Serial.print(balance);
        Serial.print(") | balance = ");
    #endif
    balance = constrain(balance, PT2314_BALANCE_MIN, PT2314_BALANCE_MAX);
    #ifdef PT2314_SERIAL_MONITOR
        Serial.println(balance);
    #endif
    
    if (balance == 0) {
        write(COMMAND_L_ATTENUATOR, 0x00);
        write(COMMAND_R_ATTENUATOR, 0x00);
    } else {
        if (balance < 0) {
            write(COMMAND_L_ATTENUATOR, 0x00);
            write(COMMAND_R_ATTENUATOR, (byte)abs(balance));
        } else {
            write(COMMAND_L_ATTENUATOR, (byte)abs(balance));
            write(COMMAND_R_ATTENUATOR, 0x00);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
// Private functions

void PT2314::write(byte command, byte val)
{    
    Wire.beginTransmission(PT2314_ADDR);
    Wire.write(command | val);
    Wire.endTransmission();
    delay(10);
}